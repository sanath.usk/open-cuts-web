/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020 Jan Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { expect } from "chai";
import { shallowMount } from "@vue/test-utils";

import _404View from "@/views/404.vue";

describe("404.vue", () => {
  it("renders 404 page", () => {
    const wrapper = shallowMount(_404View, {
      mocks: {
        $route: {
          path: "/does/not/exist",
          params: { pathMatch: "/does/not/exist" }
        }
      },
      components: {
        "router-link": {}
      }
    });

    expect(wrapper.text()).to.include("Error 404");
    expect(wrapper.text()).to.include("Sorry");
    expect(wrapper.text()).to.include("/does/not/exist");
  });
});
