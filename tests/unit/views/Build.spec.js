/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020 Jan Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import chai, { expect } from "chai";
import sinonChai from "sinon-chai";

chai.use(sinonChai);

import { shallowMount, createLocalVue } from "@vue/test-utils";
import { BootstrapVue, BootstrapVueIcons } from "bootstrap-vue";

const localVue = createLocalVue();
localVue.use(BootstrapVue);
localVue.use(BootstrapVueIcons);

import BuildView from "@/views/Build.vue";

describe("Build.vue", () => {
  it("renders build page", () => {
    const wrapper = shallowMount(BuildView, {
      localVue,
      components: {
        "router-link": {}
      },
      mocks: {
        $route: {
          params: { id: "build-id" }
        },
        $apollo: {
          queries: {
            build: { loading: false }
          }
        }
      },
      data: () => ({
        build: {
          id: "build-id",
          system: { name: "My System", id: "system-id" },
          tag: "1.3.37",
          date: "1587907707212",
          channel: "stable",
          runs: [
            {
              id: "run-id",
              date: "1587907707212",
              result: "PASS",
              test: {
                id: "test-id",
                name: "some test"
              }
            }
          ]
        }
      })
    });
    expect(wrapper.text()).to.include("My System");
    expect(wrapper.text()).to.include("1.3.37");
    expect(wrapper.text()).to.include("Released on 4/26/2020");
    expect(wrapper.text()).to.include("Channel: stable");
    expect(wrapper.text()).to.include(
      "There have been 1 runs reported for this build"
    );
  });
});
