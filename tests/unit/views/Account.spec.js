/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020 Jan Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import chai, { expect } from "chai";
import { fake } from "sinon";
import sinonChai from "sinon-chai";

chai.use(sinonChai);

import { shallowMount, createLocalVue } from "@vue/test-utils";
import { BootstrapVue, BootstrapVueIcons } from "bootstrap-vue";

const localVue = createLocalVue();
localVue.use(BootstrapVue);
localVue.use(BootstrapVueIcons);

import AccountView from "@/views/Account.vue";

describe("Account.vue", () => {
  it("renders about page if token is set", () => {
    global.localStorage = {
      getItem: fake.returns("wasd-this-is-a-token-asdf")
    };
    const wrapper = shallowMount(AccountView, {
      localVue,
      mocks: {
        $apollo: {
          queries: {
            me: { loading: false }
          }
        }
      },
      data: () => ({
        me: {
          id: "user-id",
          name: "Jan Sprinz",
          groups: ["ADMIN", "MOD", "REPORTER"],
          email: "noreply@open-cuts.org",
          joined: "1587907707212"
        }
      })
    });
    expect(global.localStorage.getItem).to.have.been.calledWith(
      "OC_AUTH_TOKEN"
    );
    expect(wrapper.text()).to.include("API token wasd-this-is-a-token-asdf");
    expect(wrapper.text()).to.include("Jan Sprinz");
    expect(wrapper.text()).to.include("ID: user-id");
    expect(wrapper.text()).to.include("Groups: ADMIN, MOD, REPORTER");
    expect(wrapper.text()).to.include("E-Mail: noreply@open-cuts.org");
    expect(wrapper.text()).to.include("Joined: 4/26/2020");
  });

  it("redirects to /login if no token is set", () => {
    global.localStorage = {
      getItem: fake.returns(null)
    };
    delete global.window.location;
    global.window.location = { href: "/account" };
    shallowMount(AccountView, {
      localVue,
      mocks: {
        $apollo: {
          queries: {
            me: { loading: false }
          }
        }
      }
    });
    expect(global.window.location.href).to.eql("/login");
  });

  it("allows logging out", async () => {
    global.localStorage = {
      getItem: fake.returns("wasd-this-is-a-token-asdf"),
      removeItem: fake()
    };
    delete global.window.location;
    global.window.location = { href: "/account" };
    const wrapper = shallowMount(AccountView, {
      localVue,
      mocks: {
        $apollo: {
          queries: {
            me: { loading: false }
          }
        }
      }
    });
    wrapper.vm.logout();

    expect(global.localStorage.removeItem).to.have.been.calledWith(
      "OC_AUTH_TOKEN"
    );
    expect(global.window.location.href).to.eql("/");
  });

  it("allows regenerating token", async () => {
    global.localStorage = {
      getItem: fake.returns("wasd-this-is-a-token-asdf"),
      setItem: fake()
    };
    const mutateFake = fake.resolves({
      data: {
        regenerateToken: "regenerated-token"
      }
    });
    delete global.window.location;
    global.window.location = { href: "" };
    const wrapper = shallowMount(AccountView, {
      localVue,
      mocks: {
        $apollo: {
          queries: {
            me: { loading: false }
          },
          mutate: mutateFake
        }
      }
    });
    await wrapper.vm.regenerate();

    expect(mutateFake).to.have.been.called;
    expect(global.localStorage.setItem).to.have.been.calledWith(
      "OC_AUTH_TOKEN",
      "regenerated-token"
    );
    expect(global.window.location.href).to.eql("/account");
  });
});
