/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020 Jan Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import Vue from "vue";
import VueRouter from "vue-router";
import Home from "./views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/report/",
    name: "Report",
    component: () =>
      import(/* webpackChunkName: "report" */ "./views/Report.vue")
  },
  {
    path: "/stats/",
    name: "Stats",
    component: () => import(/* webpackChunkName: "stats" */ "./views/Stats.vue")
  },
  {
    path: "*",
    name: "404",
    component: () => import(/* webpackChunkName: "404" */ "./views/404.vue")
  },
  {
    path: "/dashboard/",
    redirect: { name: "Home" }
  },
  {
    path: "/systems/",
    name: "Systems",
    redirect: { name: "Home" }
  },
  {
    path: "/system/:id",
    name: "System",
    component: () =>
      import(/* webpackChunkName: "system" */ "./views/System.vue")
  },
  {
    path: "/system/",
    redirect: { name: "Home" }
  },
  {
    path: "/build/:id",
    name: "Build",
    component: () => import(/* webpackChunkName: "build" */ "./views/Build.vue")
  },
  {
    path: "/build/",
    redirect: { name: "Home" }
  },
  {
    path: "/test/:id",
    name: "Test",
    component: () => import(/* webpackChunkName: "test" */ "./views/Test.vue")
  },
  {
    path: "/user/:id",
    name: "User",
    component: () => import(/* webpackChunkName: "user" */ "./views/User.vue")
  },
  {
    path: "/test/",
    redirect: { name: "Home" }
  },
  {
    path: "/run/:id",
    name: "Run",
    component: () => import(/* webpackChunkName: "run" */ "./views/Run.vue")
  },
  {
    path: "/run/",
    redirect: { name: "Home" }
  },
  {
    path: "/login",
    name: "Log in",
    component: () => import(/* webpackChunkName: "login" */ "./views/Login.vue")
  },
  {
    path: "/register",
    name: "Register",
    component: () =>
      import(/* webpackChunkName: "register" */ "./views/Register.vue")
  },
  {
    path: "/confirm",
    name: "Confirm registration",
    component: () =>
      import(
        /* webpackChunkName: "confirmRegistration" */ "./views/ConfirmRegistration.vue"
      )
  },
  {
    path: "/account",
    name: "My account",
    component: () =>
      import(/* webpackChunkName: "account" */ "./views/Account.vue")
  },
  {
    path: "/about",
    redirect: { name: "Help" }
  },
  {
    path: "/help",
    name: "Help",
    component: () => import(/* webpackChunkName: "help" */ "./views/Help.vue")
  },
  {
    path: "/privacy",
    name: "Privacy Policy",
    component: () =>
      import(/* webpackChunkName: "privacy" */ "./views/PrivacyPolicy.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
