# OPEN-CUTS web

[![UBports OPEN-CUTS](https://img.shields.io/badge/dynamic/json?label=OPEN-CUTS%20demo&color=green&prefix=users%3A%20&query=%24.data.stats.users.total&url=https%3A%2F%2Fubports.open-cuts.org%2Fgraphql%3Fquery%3D%257Bstats%2520%257Busers%2520%257Btotal%257D%257D%257D)](https://ubports.open-cuts.org)

**THIS PROJECT IS STILL IN DEVELOPMENT! DO NOT USE FOR PRODUCTION!**

The web-ui for [OPEN-CUTS, the open crowdsourced user-testing suite](https://www.open-cuts.org). Built with [Vue.js](https://vuejs.org), the [Bootstrap-Vue](https://bootstrap-vue.js.org) framework and the [Vue Apollo](https://apollo.vuejs.org/) graphql client.

## Setup

Run `npm install` to install the dependencies and create an `.env` file:

```
VUE_APP_TITLE=<title of the open-cuts installation>
VUE_APP_AUTHOR=<name of the person or project providing the open-cuts installation>
VUE_APP_EMAIL=<email-address of the person or project providing the open-cuts installation>
VUE_APP_APIURL=<url of the open-cuts graphql api>
```

The development server can be started by running `npm start`. It will be served on [http://localhost:8080/](http://localhost:8080/).

## Build for production
```
npm run build
```

## Development
```
npm run test # run unit tests
npm run lint # validate code style
npm run lint-fix # fix code style
```

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
